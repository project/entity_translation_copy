INTRODUCTION
------------

This module will give you the option to copy the values of the fields (which are
set to be translatable) into the default site language just like it would do if
you would configure the entity/node manually and select the checkbox
"copy translations".

For a full description of the module, visit the project page:
  https://www.drupal.org/project/entity_translation_copy
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/entity_translation_copy


REQUIREMENTS
------------

This module requires the following modules:

 * Entity Translation (https://drupal.org/project/entity_translation)


RECOMMENDED MODULES
-------------------

None.


INSTALLATION
------------

 * Install as usual, see http://drupal.org/node/70151 and
   https://www.drupal.org/docs/7/extend/installing-modules for further
   information.


CONFIGURATION
-------------

None.


TROUBLESHOOTING
---------------

 * https://www.drupal.org/docs/7/modules/entity-translation-copy


MAINTAINERS
-----------

Current maintainers:
 * Bram Driesen (BramDriesen) - https://www.drupal.org/u/BramDriesen
