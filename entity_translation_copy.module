<?php

/**
 * @file
 * Module file containing all the logic for the custom form & hooks.
 */

/**
 * Implements hook_help().
 */
function entity_translation_copy_help($path, $arg) {
  switch ($path) {
    case 'admin/help#entity_translation_copy':
      $filepath = dirname(__FILE__) . '/README.md';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }
      else {
        $filepath = dirname(__FILE__) . '/README.txt';
        if (file_exists($filepath)) {
          $readme = file_get_contents($filepath);
        }
      }

      if (!isset($readme)) {
        return NULL;
      }

      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}

/**
 * Implements hook_menu().
 */
function entity_translation_copy_menu() {
  $items['admin/config/regional/entity_translation_copy'] = [
    'title' => 'Entity translation copy',
    'description' => 'Copy translations from the "und" language into the default site language.',
    'page callback' => '_entity_translation_copy_overview',
    'access arguments' => ['administer entity translation'],
    'file' => 'entity_translation_copy.module',
    'module' => 'entity_translation_copy',
    'type' => MENU_NORMAL_ITEM,
  ];

  $items['admin/config/regional/entity_translation_copy/%'] = [
    'title' => 'Copy translations for a particular content type',
    'page callback' => '_entity_translation_copy_process_translations',
    'page arguments' => [4],
    'access arguments' => ['administer entity translation'],
    'file' => 'entity_translation_copy.module',
    'module' => 'entity_translation_copy',
    'type' => MENU_CALLBACK,
  ];

  return $items;
}

/**
 * Generates a tabular overview of translatable content types and their status.
 *
 * @return string
 *   Rendered HTML.
 *
 * @throws \Exception.
 */
function _entity_translation_copy_overview() {
  $list_of_copied_content_types = variable_get('entity_translation_copy_content_types_info', _entity_translation_copy_get_translatable_content_types());

  $table_headers = ['Content type', 'Data copied', ''];

  $table_rows = [];
  foreach ($list_of_copied_content_types as $content_type_name => $data_is_copied) {
    $table_rows[] = [
      $content_type_name,
      $data_is_copied ? t('Yes') : t('No'),
      _entity_translation_copy_get_action_link($data_is_copied, $content_type_name),
    ];
  }

  return theme('table', ['header' => $table_headers, 'rows' => $table_rows]);
}

/**
 * Get the button text for the overview screen.
 *
 * @param bool $data_is_copied
 *   If the data has been copied over or not.
 * @param string $content_type_name
 *   The content type machine name.
 *
 * @return string
 *   Returns the button text/link.
 */
function _entity_translation_copy_get_action_link($data_is_copied, $content_type_name) {
  if ($data_is_copied) {
    return t('Data already copied.', [], ['context' => 'Entity Translation Copy']);
  }
  return l(t('Copy data', [], ['context' => 'Entity Translation Copy']), 'admin/config/regional/entity_translation_copy/' . $content_type_name);
}

/**
 * Get a list of translatable content types.
 *
 * @return array
 *   Array of translatable content types, each value initialized to FALSE.
 */
function _entity_translation_copy_get_translatable_content_types() {
  $translatable_content_types = [];

  // Get a list of all content types and go over them.
  foreach (node_type_get_types() as $content_type_name => $content_type) {
    $translatable_content_types[$content_type_name] = FALSE;
  }

  return $translatable_content_types;
}

/**
 * Given a content type name, will start a batch to process all data.
 *
 * This will copy over the LANGUAGE_NONE fields into all other languages.
 *
 * @param string $content_type_name
 *   Machine name of the content type.
 */
function _entity_translation_copy_process_translations($content_type_name) {
  $translatable_content_types_info = variable_get('entity_translation_copy_content_types_info', _entity_translation_copy_get_translatable_content_types());

  // If the item is already translated, we just go back to the overview page.
  if ($translatable_content_types_info[$content_type_name]) {
    drupal_goto('admin/config/regional/entity_translation_copy');
  }

  // Create a batch with all the requited information.
  $batch = [
    'title' => t('Translate the %content_type content type.', ['%content_type' => $content_type_name], ['context' => 'Entity Translation Copy']),
    'operations' => [
      ['_entity_translation_copy_process_translations_batch', [$content_type_name]],
    ],
    'finished' => '_entity_translation_copy_process_translations_finished',
    'file' => drupal_get_path('module', 'entity_translation') . '/entity_translation.admin.inc',
  ];

  // Set the batch in the queue and start processing it.
  batch_set($batch);
  batch_process();
}

/**
 * Batch operations callback for processing entity translations.
 *
 * @param string $content_type_name
 *   Machine name of the content type.
 * @param array $context
 *   Batch context.
 */
function _entity_translation_copy_process_translations_batch($content_type_name, array &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    // Init the processed nodes array.
    $context['results']['processed_node_ids'] = [];

    // Get the node IDs of this content type.
    $node_ids = db_query('SELECT nid FROM {node} WHERE type = :type',
      [':type' => $content_type_name])->fetchCol();

    $context['sandbox']['nids'] = $node_ids;
    $context['sandbox']['max'] = count($node_ids);
  }

  // Go through a batch of node IDs.
  $limit_per_batch = 50;
  for ($iterator = $context['sandbox']['progress']; ($iterator < $context['sandbox']['max']) && ($iterator < ($context['sandbox']['progress'] + $limit_per_batch)); $iterator++) {
    $node_id = $context['sandbox']['nids'][$iterator];
    // Call entity_translation's function used when loading the "Translate" tab.
    // This is the only consistent way of making translations show up for all
    // languages.
    entity_translation_overview('node', $node_id);
    // Make sure everything is stored correctly by loading and saving the node
    // like it's being loaded.
    try {
      $node = node_load($node_id);
      node_save($node);
    }
    catch (Exception $exception) {
      // Continue if failed.
      continue;
    }
    // Store some result for post-processing in the finished callback.
    $context['results']['processed_node_ids'][] = $node_id;
  }

  // Update our progress.
  $context['sandbox']['progress'] += $limit_per_batch;
  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];

  // Ensure we know the content type name in the batch finished callback.
  $context['results']['content_type_name'] = $content_type_name;
}

/**
 * Batch 'finished' callback.
 *
 * Updates a variable that keeps track of all translatable content types whose
 * translations have been copied over.
 *
 * @param bool $success
 *   Status of the batch.
 * @param array $results
 *   Results of the batch.
 * @param array $operations
 *   Executed operations.
 */
function _entity_translation_copy_process_translations_finished($success, array $results, array $operations) {
  $translatable_content_types_info = variable_get('entity_translation_copy_content_types_info', _entity_translation_copy_get_translatable_content_types());

  // Get the processed content type.
  $processed_content_type = $results['content_type_name'];
  // Mark that the translations of this content type have been copied over.
  if (is_array($translatable_content_types_info)) {
    // We use the $success variable here, in case the batch happens to fail,
    // the user can then still try to retry the operations.
    $translatable_content_types_info[$processed_content_type] = $success;
    variable_set('entity_translation_copy_content_types_info', $translatable_content_types_info);
  }

  if ($success) {
    // Display a message in the UI if everything went fine.
    drupal_set_message(t('Successfully processed @count items for entity: %entity_type',
      [
        '@count' => count($results['processed_node_ids']),
        '%entity_type' => $processed_content_type,
      ],
      ['context' => 'Entity Translation Copy'],
    ));
  }
  else {
    // An error occurred. $operations contains the operations that remained
    // unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ], ['context' => 'Entity Translation Copy']);
    drupal_set_message($message, 'error');
  }
}

/**
 * Implements hook_node_type_insert().
 */
function entity_translation_copy_node_type_insert($info) {
  $translatable_content_types_info = variable_get('entity_translation_copy_content_types_info', _entity_translation_copy_get_translatable_content_types());
  // Initialise the content type as false.
  $translatable_content_types_info[$info->type] = FALSE;
  variable_set('entity_translation_copy_content_types_info', $translatable_content_types_info);
}

/**
 * Implements hook_node_type_delete().
 */
function entity_translation_copy_node_type_delete($info) {
  $translatable_content_types_info = variable_get('entity_translation_copy_content_types_info', _entity_translation_copy_get_translatable_content_types());
  // Delete the key from the configuration array if it's set.
  if (isset($translatable_content_types_info[$info->type])) {
    unset($translatable_content_types_info[$info->type]);
    variable_set('entity_translation_copy_content_types_info', $translatable_content_types_info);
  }
}
